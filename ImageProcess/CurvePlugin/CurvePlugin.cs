﻿using Common;
using System.Windows.Forms;

namespace CurveAdjustPlugin
{
    public class CurvePlugin : IProcessorPlugin
    {
        #region IProcessorPlugin Members
        ImageProcessApp app = null;
        public void Load(ImageProcessApp app)
        {
            this.app = app;
        }

        public void Unload()
        {
        
        }

        public void ProcessCommand(string commandName, Document doc)
        {
            if (commandName == "Curve")
            {
                CurveAdjust(doc);
            }
        }

        private void CurveAdjust(Document doc)
        {
            CurveForm form = new CurveForm();
            form.SetBitmap(doc.GetBitmap());
            if (form.ShowDialog() == DialogResult.OK)
            {
                doc.SetBitmap(form.GetBitmap());
                app.ImageView.Refresh();
            }
        }

        public void RegisterMenu(IMenuRegistry registry)
        {
            registry.RegisterMenuItem("&Adjust", "&Curve...", "Curve",Keys.Control|Keys.M, this);
        }

        #endregion
    }
}
