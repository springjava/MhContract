﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Sbk_Manage
{
    public class SimpleInstanceApplication : Microsoft.VisualBasic.ApplicationServices.WindowsFormsApplicationBase
    {
        public SimpleInstanceApplication()
        {
            this.IsSingleInstance = true;
            this.EnableVisualStyles = true;
            this.SaveMySettingsOnExit = true;
            this.ShutdownStyle = Microsoft.VisualBasic.ApplicationServices.ShutdownMode.AfterMainFormCloses;
        }

        protected override bool OnStartup(Microsoft.VisualBasic.ApplicationServices.StartupEventArgs eventArgs)
        {
            string[] args = new string[eventArgs.CommandLine.Count];
            eventArgs.CommandLine.CopyTo(args, 0);
            FrmMain.args = args;
            Program.FrmMainIns = new FrmMain();
            Application.Run(Program.FrmMainIns);
            return false;
        }
        protected override void OnStartupNextInstance(Microsoft.VisualBasic.ApplicationServices.StartupNextInstanceEventArgs eventArgs)
        {
             if(Program.FrmMainIns!=null){
                if (!Program.FrmMainIns.Visible) {
                    Program.FrmMainIns.Show();
                    Program.FrmMainIns.ShowInTaskbar = true;
                }
                if(Program.FrmMainIns.WindowState == FormWindowState.Minimized){
                    Program.FrmMainIns.WindowState = FormWindowState.Maximized;
                }
                Program.FrmMainIns.BringToFront();
                Program.FrmMainIns.Activate();
             }
        }
    }
}
