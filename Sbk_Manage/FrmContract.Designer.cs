﻿namespace Sbk_Manage
{
    partial class FrmContract
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmContract));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ToolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.SplitContainer1 = new System.Windows.Forms.SplitContainer();
            this.TableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.TxtHtlrnf = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.dtpHtQdrq = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Txtghsyhk = new System.Windows.Forms.TextBox();
            this.Txtghs = new System.Windows.Forms.TextBox();
            this.BtnQuery = new System.Windows.Forms.Button();
            this.TxtHtBh = new System.Windows.Forms.TextBox();
            this.TxtXmBh = new System.Windows.Forms.TextBox();
            this.TxtZbdlgs = new System.Windows.Forms.TextBox();
            this.ChkHtqdrq = new System.Windows.Forms.CheckBox();
            this.ChkHtysrq = new System.Windows.Forms.CheckBox();
            this.TxtHte = new System.Windows.Forms.TextBox();
            this.BtnPreview = new System.Windows.Forms.Button();
            this.BtnNext = new System.Windows.Forms.Button();
            this.BtnReset = new System.Windows.Forms.Button();
            this.TxtHtmc = new System.Windows.Forms.TextBox();
            this.DtpHtYsrq = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.DgvHtInfo = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scan_flag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jc_flag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.htbh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xmbh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.htmc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zbdlgs = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ghs = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ghskpxx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.htqdrq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.htysrq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.htlrrq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.htlrr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStrip1 = new System.Windows.Forms.ToolStrip();
            this.AddContractBtn = new System.Windows.Forms.ToolStripButton();
            this.EditContractBtn = new System.Windows.Forms.ToolStripButton();
            this.DelContractBtn = new System.Windows.Forms.ToolStripButton();
            this.BarcodePrintBtn = new System.Windows.Forms.ToolStripButton();
            this.JieYBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.ExportExcelBtn = new System.Windows.Forms.ToolStripButton();
            this.StatusStrip1.SuspendLayout();
            this.SplitContainer1.Panel1.SuspendLayout();
            this.SplitContainer1.Panel2.SuspendLayout();
            this.SplitContainer1.SuspendLayout();
            this.TableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvHtInfo)).BeginInit();
            this.ToolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripStatusLabel1});
            this.StatusStrip1.Location = new System.Drawing.Point(0, 505);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.StatusStrip1.Size = new System.Drawing.Size(1326, 22);
            this.StatusStrip1.SizingGrip = false;
            this.StatusStrip1.Stretch = false;
            this.StatusStrip1.TabIndex = 2;
            this.StatusStrip1.Text = "StatusStrip1";
            // 
            // ToolStripStatusLabel1
            // 
            this.ToolStripStatusLabel1.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripStatusLabel1.Image")));
            this.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1";
            this.ToolStripStatusLabel1.Size = new System.Drawing.Size(16, 17);
            // 
            // SplitContainer1
            // 
            this.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.SplitContainer1.IsSplitterFixed = true;
            this.SplitContainer1.Location = new System.Drawing.Point(0, 70);
            this.SplitContainer1.Margin = new System.Windows.Forms.Padding(4);
            this.SplitContainer1.Name = "SplitContainer1";
            this.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // SplitContainer1.Panel1
            // 
            this.SplitContainer1.Panel1.Controls.Add(this.TableLayoutPanel1);
            // 
            // SplitContainer1.Panel2
            // 
            this.SplitContainer1.Panel2.Controls.Add(this.DgvHtInfo);
            this.SplitContainer1.Size = new System.Drawing.Size(1326, 435);
            this.SplitContainer1.SplitterDistance = 152;
            this.SplitContainer1.SplitterWidth = 1;
            this.SplitContainer1.TabIndex = 3;
            this.SplitContainer1.TabStop = false;
            // 
            // TableLayoutPanel1
            // 
            this.TableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Outset;
            this.TableLayoutPanel1.ColumnCount = 6;
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TableLayoutPanel1.Controls.Add(this.TxtHtlrnf, 4, 1);
            this.TableLayoutPanel1.Controls.Add(this.label8, 4, 0);
            this.TableLayoutPanel1.Controls.Add(this.dtpHtQdrq, 3, 3);
            this.TableLayoutPanel1.Controls.Add(this.label7, 2, 2);
            this.TableLayoutPanel1.Controls.Add(this.label6, 1, 2);
            this.TableLayoutPanel1.Controls.Add(this.label5, 0, 2);
            this.TableLayoutPanel1.Controls.Add(this.label4, 3, 0);
            this.TableLayoutPanel1.Controls.Add(this.label3, 2, 0);
            this.TableLayoutPanel1.Controls.Add(this.label2, 1, 0);
            this.TableLayoutPanel1.Controls.Add(this.Txtghsyhk, 0, 3);
            this.TableLayoutPanel1.Controls.Add(this.Txtghs, 0, 3);
            this.TableLayoutPanel1.Controls.Add(this.BtnQuery, 5, 0);
            this.TableLayoutPanel1.Controls.Add(this.TxtHtBh, 0, 1);
            this.TableLayoutPanel1.Controls.Add(this.TxtXmBh, 1, 1);
            this.TableLayoutPanel1.Controls.Add(this.TxtZbdlgs, 2, 1);
            this.TableLayoutPanel1.Controls.Add(this.ChkHtqdrq, 3, 2);
            this.TableLayoutPanel1.Controls.Add(this.ChkHtysrq, 4, 2);
            this.TableLayoutPanel1.Controls.Add(this.TxtHte, 0, 3);
            this.TableLayoutPanel1.Controls.Add(this.BtnPreview, 5, 1);
            this.TableLayoutPanel1.Controls.Add(this.BtnNext, 5, 2);
            this.TableLayoutPanel1.Controls.Add(this.BtnReset, 5, 3);
            this.TableLayoutPanel1.Controls.Add(this.TxtHtmc, 3, 1);
            this.TableLayoutPanel1.Controls.Add(this.DtpHtYsrq, 4, 3);
            this.TableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.TableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.TableLayoutPanel1.Name = "TableLayoutPanel1";
            this.TableLayoutPanel1.RowCount = 4;
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.TableLayoutPanel1.Size = new System.Drawing.Size(1322, 148);
            this.TableLayoutPanel1.TabIndex = 0;
            // 
            // TxtHtlrnf
            // 
            this.TxtHtlrnf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtHtlrnf.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TxtHtlrnf.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TxtHtlrnf.Location = new System.Drawing.Point(882, 42);
            this.TxtHtlrnf.Margin = new System.Windows.Forms.Padding(4);
            this.TxtHtlrnf.MaxLength = 4;
            this.TxtHtlrnf.Name = "TxtHtlrnf";
            this.TxtHtlrnf.Size = new System.Drawing.Size(209, 26);
            this.TxtHtlrnf.TabIndex = 5;
            this.TxtHtlrnf.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(881, 2);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(211, 34);
            this.label8.TabIndex = 35;
            this.label8.Text = "合同录入年份";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtpHtQdrq
            // 
            this.dtpHtQdrq.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dtpHtQdrq.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dtpHtQdrq.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpHtQdrq.Location = new System.Drawing.Point(663, 116);
            this.dtpHtQdrq.Margin = new System.Windows.Forms.Padding(4);
            this.dtpHtQdrq.Name = "dtpHtQdrq";
            this.dtpHtQdrq.Size = new System.Drawing.Size(209, 26);
            this.dtpHtQdrq.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(443, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(211, 34);
            this.label7.TabIndex = 33;
            this.label7.Text = "合同额";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(224, 74);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(211, 34);
            this.label6.TabIndex = 32;
            this.label6.Text = "供货商银行账户";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(5, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(211, 34);
            this.label5.TabIndex = 31;
            this.label5.Text = "供货商";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(662, 2);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(211, 34);
            this.label4.TabIndex = 30;
            this.label4.Text = "合同名称";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(443, 2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(211, 34);
            this.label3.TabIndex = 29;
            this.label3.Text = "招标代理公司";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(224, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(211, 34);
            this.label2.TabIndex = 28;
            this.label2.Text = "项目编号";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Txtghsyhk
            // 
            this.Txtghsyhk.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtghsyhk.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Txtghsyhk.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Txtghsyhk.Location = new System.Drawing.Point(225, 116);
            this.Txtghsyhk.Margin = new System.Windows.Forms.Padding(4);
            this.Txtghsyhk.MaxLength = 100;
            this.Txtghsyhk.Name = "Txtghsyhk";
            this.Txtghsyhk.Size = new System.Drawing.Size(209, 26);
            this.Txtghsyhk.TabIndex = 7;
            this.Txtghsyhk.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Txtghs
            // 
            this.Txtghs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtghs.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Txtghs.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Txtghs.Location = new System.Drawing.Point(6, 116);
            this.Txtghs.Margin = new System.Windows.Forms.Padding(4);
            this.Txtghs.MaxLength = 100;
            this.Txtghs.Name = "Txtghs";
            this.Txtghs.Size = new System.Drawing.Size(209, 26);
            this.Txtghs.TabIndex = 6;
            this.Txtghs.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // BtnQuery
            // 
            this.BtnQuery.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BtnQuery.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnQuery.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnQuery.Image = ((System.Drawing.Image)(resources.GetObject("BtnQuery.Image")));
            this.BtnQuery.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnQuery.Location = new System.Drawing.Point(1101, 6);
            this.BtnQuery.Margin = new System.Windows.Forms.Padding(4);
            this.BtnQuery.Name = "BtnQuery";
            this.BtnQuery.Size = new System.Drawing.Size(215, 26);
            this.BtnQuery.TabIndex = 13;
            this.BtnQuery.Text = "查询";
            this.BtnQuery.UseVisualStyleBackColor = true;
            this.BtnQuery.Click += new System.EventHandler(this.BtnQuery_Click);
            // 
            // TxtHtBh
            // 
            this.TxtHtBh.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtHtBh.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TxtHtBh.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TxtHtBh.Location = new System.Drawing.Point(6, 42);
            this.TxtHtBh.Margin = new System.Windows.Forms.Padding(4);
            this.TxtHtBh.MaxLength = 100;
            this.TxtHtBh.Name = "TxtHtBh";
            this.TxtHtBh.Size = new System.Drawing.Size(209, 26);
            this.TxtHtBh.TabIndex = 1;
            this.TxtHtBh.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TxtXmBh
            // 
            this.TxtXmBh.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtXmBh.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TxtXmBh.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TxtXmBh.Location = new System.Drawing.Point(225, 42);
            this.TxtXmBh.Margin = new System.Windows.Forms.Padding(4);
            this.TxtXmBh.MaxLength = 100;
            this.TxtXmBh.Name = "TxtXmBh";
            this.TxtXmBh.Size = new System.Drawing.Size(209, 26);
            this.TxtXmBh.TabIndex = 2;
            this.TxtXmBh.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TxtZbdlgs
            // 
            this.TxtZbdlgs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtZbdlgs.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TxtZbdlgs.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TxtZbdlgs.Location = new System.Drawing.Point(444, 42);
            this.TxtZbdlgs.Margin = new System.Windows.Forms.Padding(4);
            this.TxtZbdlgs.MaxLength = 100;
            this.TxtZbdlgs.Name = "TxtZbdlgs";
            this.TxtZbdlgs.Size = new System.Drawing.Size(209, 26);
            this.TxtZbdlgs.TabIndex = 3;
            this.TxtZbdlgs.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ChkHtqdrq
            // 
            this.ChkHtqdrq.AutoSize = true;
            this.ChkHtqdrq.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ChkHtqdrq.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ChkHtqdrq.Location = new System.Drawing.Point(663, 78);
            this.ChkHtqdrq.Margin = new System.Windows.Forms.Padding(4);
            this.ChkHtqdrq.Name = "ChkHtqdrq";
            this.ChkHtqdrq.Size = new System.Drawing.Size(209, 26);
            this.ChkHtqdrq.TabIndex = 9;
            this.ChkHtqdrq.Text = "合同签订日期";
            this.ChkHtqdrq.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ChkHtqdrq.UseVisualStyleBackColor = true;
            // 
            // ChkHtysrq
            // 
            this.ChkHtysrq.AutoSize = true;
            this.ChkHtysrq.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ChkHtysrq.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ChkHtysrq.Location = new System.Drawing.Point(882, 78);
            this.ChkHtysrq.Margin = new System.Windows.Forms.Padding(4);
            this.ChkHtysrq.Name = "ChkHtysrq";
            this.ChkHtysrq.Size = new System.Drawing.Size(209, 26);
            this.ChkHtysrq.TabIndex = 11;
            this.ChkHtysrq.Text = "合同验收日期";
            this.ChkHtysrq.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ChkHtysrq.UseVisualStyleBackColor = true;
            // 
            // TxtHte
            // 
            this.TxtHte.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtHte.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TxtHte.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TxtHte.Location = new System.Drawing.Point(444, 116);
            this.TxtHte.Margin = new System.Windows.Forms.Padding(4);
            this.TxtHte.MaxLength = 100;
            this.TxtHte.Name = "TxtHte";
            this.TxtHte.Size = new System.Drawing.Size(209, 26);
            this.TxtHte.TabIndex = 8;
            this.TxtHte.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // BtnPreview
            // 
            this.BtnPreview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BtnPreview.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnPreview.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnPreview.Image = ((System.Drawing.Image)(resources.GetObject("BtnPreview.Image")));
            this.BtnPreview.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnPreview.Location = new System.Drawing.Point(1101, 42);
            this.BtnPreview.Margin = new System.Windows.Forms.Padding(4);
            this.BtnPreview.Name = "BtnPreview";
            this.BtnPreview.Size = new System.Drawing.Size(215, 26);
            this.BtnPreview.TabIndex = 14;
            this.BtnPreview.Text = "上一页";
            this.BtnPreview.UseVisualStyleBackColor = true;
            this.BtnPreview.Click += new System.EventHandler(this.BtnPreview_Click);
            // 
            // BtnNext
            // 
            this.BtnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BtnNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnNext.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnNext.Image = ((System.Drawing.Image)(resources.GetObject("BtnNext.Image")));
            this.BtnNext.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnNext.Location = new System.Drawing.Point(1101, 78);
            this.BtnNext.Margin = new System.Windows.Forms.Padding(4);
            this.BtnNext.Name = "BtnNext";
            this.BtnNext.Size = new System.Drawing.Size(215, 26);
            this.BtnNext.TabIndex = 15;
            this.BtnNext.Text = "下一页";
            this.BtnNext.UseVisualStyleBackColor = true;
            this.BtnNext.Click += new System.EventHandler(this.BtnNext_Click);
            // 
            // BtnReset
            // 
            this.BtnReset.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BtnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnReset.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnReset.Image = ((System.Drawing.Image)(resources.GetObject("BtnReset.Image")));
            this.BtnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnReset.Location = new System.Drawing.Point(1101, 114);
            this.BtnReset.Margin = new System.Windows.Forms.Padding(4);
            this.BtnReset.Name = "BtnReset";
            this.BtnReset.Size = new System.Drawing.Size(215, 28);
            this.BtnReset.TabIndex = 16;
            this.BtnReset.Text = "重置查询条件";
            this.BtnReset.UseVisualStyleBackColor = true;
            this.BtnReset.Click += new System.EventHandler(this.BtnReset_Click);
            // 
            // TxtHtmc
            // 
            this.TxtHtmc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtHtmc.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TxtHtmc.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TxtHtmc.Location = new System.Drawing.Point(663, 42);
            this.TxtHtmc.Margin = new System.Windows.Forms.Padding(4);
            this.TxtHtmc.MaxLength = 100;
            this.TxtHtmc.Name = "TxtHtmc";
            this.TxtHtmc.Size = new System.Drawing.Size(209, 26);
            this.TxtHtmc.TabIndex = 4;
            this.TxtHtmc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // DtpHtYsrq
            // 
            this.DtpHtYsrq.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.DtpHtYsrq.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.DtpHtYsrq.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DtpHtYsrq.Location = new System.Drawing.Point(882, 116);
            this.DtpHtYsrq.Margin = new System.Windows.Forms.Padding(4);
            this.DtpHtYsrq.Name = "DtpHtYsrq";
            this.DtpHtYsrq.Size = new System.Drawing.Size(209, 26);
            this.DtpHtYsrq.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(5, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(211, 34);
            this.label1.TabIndex = 27;
            this.label1.Text = "合同编号";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DgvHtInfo
            // 
            this.DgvHtInfo.AllowUserToAddRows = false;
            this.DgvHtInfo.AllowUserToDeleteRows = false;
            this.DgvHtInfo.AllowUserToResizeColumns = false;
            this.DgvHtInfo.AllowUserToResizeRows = false;
            this.DgvHtInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.DgvHtInfo.BackgroundColor = System.Drawing.Color.White;
            this.DgvHtInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DgvHtInfo.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvHtInfo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DgvHtInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.scan_flag,
            this.jc_flag,
            this.htbh,
            this.xmbh,
            this.htmc,
            this.hte,
            this.zbdlgs,
            this.ghs,
            this.ghskpxx,
            this.htqdrq,
            this.htysrq,
            this.htlrrq,
            this.htlrr});
            this.DgvHtInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvHtInfo.Location = new System.Drawing.Point(0, 0);
            this.DgvHtInfo.Margin = new System.Windows.Forms.Padding(4);
            this.DgvHtInfo.MultiSelect = false;
            this.DgvHtInfo.Name = "DgvHtInfo";
            this.DgvHtInfo.ReadOnly = true;
            this.DgvHtInfo.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.DgvHtInfo.RowHeadersWidth = 45;
            this.DgvHtInfo.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DgvHtInfo.RowTemplate.Height = 23;
            this.DgvHtInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvHtInfo.Size = new System.Drawing.Size(1322, 278);
            this.DgvHtInfo.TabIndex = 0;
            this.DgvHtInfo.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.DgvHtInfo_CellFormatting);
            this.DgvHtInfo.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DgvHtInfo_CellMouseClick);
            this.DgvHtInfo.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DgvHtInfo_CellMouseDoubleClick);
            this.DgvHtInfo.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.DgvHtInfo_DataBindingComplete);
            this.DgvHtInfo.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.DgvHtInfo_RowPostPaint);
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "合同ID";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            this.id.Width = 81;
            // 
            // scan_flag
            // 
            this.scan_flag.DataPropertyName = "scan_flag";
            this.scan_flag.HeaderText = "扫描";
            this.scan_flag.Name = "scan_flag";
            this.scan_flag.ReadOnly = true;
            this.scan_flag.Width = 65;
            // 
            // jc_flag
            // 
            this.jc_flag.DataPropertyName = "jc_flag";
            this.jc_flag.HeaderText = "借出";
            this.jc_flag.Name = "jc_flag";
            this.jc_flag.ReadOnly = true;
            this.jc_flag.Width = 65;
            // 
            // htbh
            // 
            this.htbh.DataPropertyName = "htbh";
            this.htbh.HeaderText = "合同编号";
            this.htbh.Name = "htbh";
            this.htbh.ReadOnly = true;
            this.htbh.Width = 97;
            // 
            // xmbh
            // 
            this.xmbh.DataPropertyName = "xmbh";
            this.xmbh.HeaderText = "项目编号";
            this.xmbh.Name = "xmbh";
            this.xmbh.ReadOnly = true;
            this.xmbh.Width = 97;
            // 
            // htmc
            // 
            this.htmc.DataPropertyName = "htmc";
            this.htmc.HeaderText = "合同名称";
            this.htmc.Name = "htmc";
            this.htmc.ReadOnly = true;
            this.htmc.Width = 97;
            // 
            // hte
            // 
            this.hte.DataPropertyName = "hte";
            this.hte.HeaderText = "合同额(万¥)";
            this.hte.Name = "hte";
            this.hte.ReadOnly = true;
            this.hte.Width = 121;
            // 
            // zbdlgs
            // 
            this.zbdlgs.DataPropertyName = "zbdlgs";
            this.zbdlgs.HeaderText = "招标代理公司";
            this.zbdlgs.Name = "zbdlgs";
            this.zbdlgs.ReadOnly = true;
            this.zbdlgs.Width = 129;
            // 
            // ghs
            // 
            this.ghs.DataPropertyName = "ghs";
            this.ghs.HeaderText = "供货商";
            this.ghs.Name = "ghs";
            this.ghs.ReadOnly = true;
            this.ghs.Width = 81;
            // 
            // ghskpxx
            // 
            this.ghskpxx.DataPropertyName = "ghskpxx";
            this.ghskpxx.HeaderText = "供货商银行账户";
            this.ghskpxx.Name = "ghskpxx";
            this.ghskpxx.ReadOnly = true;
            this.ghskpxx.Width = 145;
            // 
            // htqdrq
            // 
            this.htqdrq.DataPropertyName = "htqdrq";
            this.htqdrq.HeaderText = "合同签订日期";
            this.htqdrq.MaxInputLength = 10;
            this.htqdrq.Name = "htqdrq";
            this.htqdrq.ReadOnly = true;
            this.htqdrq.Width = 129;
            // 
            // htysrq
            // 
            this.htysrq.DataPropertyName = "htysrq";
            this.htysrq.HeaderText = "合同验收日期";
            this.htysrq.MaxInputLength = 10;
            this.htysrq.Name = "htysrq";
            this.htysrq.ReadOnly = true;
            this.htysrq.Width = 129;
            // 
            // htlrrq
            // 
            this.htlrrq.DataPropertyName = "htlrrq";
            this.htlrrq.HeaderText = "合同录入日期";
            this.htlrrq.MaxInputLength = 19;
            this.htlrrq.Name = "htlrrq";
            this.htlrrq.ReadOnly = true;
            this.htlrrq.Width = 129;
            // 
            // htlrr
            // 
            this.htlrr.DataPropertyName = "htlrr";
            this.htlrr.HeaderText = "合同录入人";
            this.htlrr.Name = "htlrr";
            this.htlrr.ReadOnly = true;
            this.htlrr.Width = 113;
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 70);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 70);
            // 
            // ToolStrip1
            // 
            this.ToolStrip1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.ToolStrip1.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.ToolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddContractBtn,
            this.EditContractBtn,
            this.DelContractBtn,
            this.ToolStripSeparator1,
            this.BarcodePrintBtn,
            this.ToolStripSeparator3,
            this.JieYBtn,
            this.toolStripSeparator2,
            this.ExportExcelBtn});
            this.ToolStrip1.Location = new System.Drawing.Point(0, 0);
            this.ToolStrip1.Name = "ToolStrip1";
            this.ToolStrip1.Size = new System.Drawing.Size(1326, 70);
            this.ToolStrip1.TabIndex = 1;
            this.ToolStrip1.Text = "ToolStrip1";
            // 
            // AddContractBtn
            // 
            this.AddContractBtn.Image = ((System.Drawing.Image)(resources.GetObject("AddContractBtn.Image")));
            this.AddContractBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AddContractBtn.Name = "AddContractBtn";
            this.AddContractBtn.Size = new System.Drawing.Size(63, 67);
            this.AddContractBtn.Tag = "";
            this.AddContractBtn.Text = "添加合同";
            this.AddContractBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.AddContractBtn.Click += new System.EventHandler(this.AddContractBtn_Click);
            // 
            // EditContractBtn
            // 
            this.EditContractBtn.Image = ((System.Drawing.Image)(resources.GetObject("EditContractBtn.Image")));
            this.EditContractBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.EditContractBtn.Name = "EditContractBtn";
            this.EditContractBtn.Size = new System.Drawing.Size(63, 67);
            this.EditContractBtn.Tag = "";
            this.EditContractBtn.Text = "编辑合同";
            this.EditContractBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.EditContractBtn.Click += new System.EventHandler(this.EditContractBtn_Click);
            // 
            // DelContractBtn
            // 
            this.DelContractBtn.Image = ((System.Drawing.Image)(resources.GetObject("DelContractBtn.Image")));
            this.DelContractBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.DelContractBtn.Name = "DelContractBtn";
            this.DelContractBtn.Size = new System.Drawing.Size(63, 67);
            this.DelContractBtn.Tag = "";
            this.DelContractBtn.Text = "删除合同";
            this.DelContractBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.DelContractBtn.Click += new System.EventHandler(this.DelContractBtn_Click);
            // 
            // BarcodePrintBtn
            // 
            this.BarcodePrintBtn.Image = global::Sbk_Manage.Properties.Resources.Scan;
            this.BarcodePrintBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BarcodePrintBtn.Name = "BarcodePrintBtn";
            this.BarcodePrintBtn.Size = new System.Drawing.Size(63, 67);
            this.BarcodePrintBtn.Tag = "";
            this.BarcodePrintBtn.Text = "合同扫描";
            this.BarcodePrintBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.BarcodePrintBtn.Click += new System.EventHandler(this.BarcodePrintBtn_Click);
            // 
            // JieYBtn
            // 
            this.JieYBtn.Image = global::Sbk_Manage.Properties.Resources.Processdata;
            this.JieYBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.JieYBtn.Name = "JieYBtn";
            this.JieYBtn.Size = new System.Drawing.Size(63, 67);
            this.JieYBtn.Tag = "";
            this.JieYBtn.Text = "合同借阅";
            this.JieYBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.JieYBtn.Click += new System.EventHandler(this.JieYBtn_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 70);
            // 
            // ExportExcelBtn
            // 
            this.ExportExcelBtn.Image = global::Sbk_Manage.Properties.Resources.Excel_2013_64px_1180012_easyicon_net;
            this.ExportExcelBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ExportExcelBtn.Name = "ExportExcelBtn";
            this.ExportExcelBtn.Size = new System.Drawing.Size(63, 67);
            this.ExportExcelBtn.Tag = "";
            this.ExportExcelBtn.Text = "合同导出";
            this.ExportExcelBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.ExportExcelBtn.ToolTipText = "合同导出EXCEL";
            this.ExportExcelBtn.Click += new System.EventHandler(this.ExportExcelBtn_Click);
            // 
            // FrmContract
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1326, 527);
            this.Controls.Add(this.SplitContainer1);
            this.Controls.Add(this.StatusStrip1);
            this.Controls.Add(this.ToolStrip1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmContract";
            this.Text = "合同管理";
            this.Load += new System.EventHandler(this.FrmContract_Load);
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            this.SplitContainer1.Panel1.ResumeLayout(false);
            this.SplitContainer1.Panel2.ResumeLayout(false);
            this.SplitContainer1.ResumeLayout(false);
            this.TableLayoutPanel1.ResumeLayout(false);
            this.TableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvHtInfo)).EndInit();
            this.ToolStrip1.ResumeLayout(false);
            this.ToolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.StatusStrip StatusStrip1;
        internal System.Windows.Forms.ToolStripStatusLabel ToolStripStatusLabel1;
        internal System.Windows.Forms.SplitContainer SplitContainer1;
        internal System.Windows.Forms.TableLayoutPanel TableLayoutPanel1;
        internal System.Windows.Forms.Button BtnQuery;
        internal System.Windows.Forms.TextBox TxtHtBh;
        internal System.Windows.Forms.TextBox TxtXmBh;
        internal System.Windows.Forms.TextBox TxtZbdlgs;
        internal System.Windows.Forms.CheckBox ChkHtysrq;
        internal System.Windows.Forms.TextBox TxtHte;
        internal System.Windows.Forms.TextBox TxtHtmc;
        internal System.Windows.Forms.Button BtnPreview;
        internal System.Windows.Forms.Button BtnNext;
        internal System.Windows.Forms.Button BtnReset;
        internal System.Windows.Forms.DateTimePicker DtpHtYsrq;
        internal System.Windows.Forms.DataGridView DgvHtInfo;
        internal System.Windows.Forms.ToolStripButton AddContractBtn;
        internal System.Windows.Forms.ToolStripButton EditContractBtn;
        internal System.Windows.Forms.ToolStripButton DelContractBtn;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton BarcodePrintBtn;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton ExportExcelBtn;
        internal System.Windows.Forms.ToolStrip ToolStrip1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton JieYBtn;
        internal System.Windows.Forms.TextBox Txtghsyhk;
        internal System.Windows.Forms.TextBox Txtghs;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        internal System.Windows.Forms.CheckBox ChkHtqdrq;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.DateTimePicker dtpHtQdrq;
        internal System.Windows.Forms.TextBox TxtHtlrnf;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn scan_flag;
        private System.Windows.Forms.DataGridViewTextBoxColumn jc_flag;
        private System.Windows.Forms.DataGridViewTextBoxColumn htbh;
        private System.Windows.Forms.DataGridViewTextBoxColumn xmbh;
        private System.Windows.Forms.DataGridViewTextBoxColumn htmc;
        private System.Windows.Forms.DataGridViewTextBoxColumn hte;
        private System.Windows.Forms.DataGridViewTextBoxColumn zbdlgs;
        private System.Windows.Forms.DataGridViewTextBoxColumn ghs;
        private System.Windows.Forms.DataGridViewTextBoxColumn ghskpxx;
        private System.Windows.Forms.DataGridViewTextBoxColumn htqdrq;
        private System.Windows.Forms.DataGridViewTextBoxColumn htysrq;
        private System.Windows.Forms.DataGridViewTextBoxColumn htlrrq;
        private System.Windows.Forms.DataGridViewTextBoxColumn htlrr;
    }
}